package com.folcademy.apiMarcket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiMarcketApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiMarcketApplication.class, args);
	}

}
